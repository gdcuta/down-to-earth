//is_place_meeting_platform(xx, yy, xx_add, yy_add)

/* Returns true if there is a collision with a solid wall or oneway (when either still or moving downwards)
    * if the calling object is put at the coordinate (xx+xx_add,yy+yy_add). Otherwise return false.
    *
    * -Note that this does not actually move the calling object. 
    * -Note that the obj_wall and obj_oneway variables are local to the calling object, not this script,
    *  meaning they need to be defined beforehand.
    */
var xx, yy, xx_add, yy_add, is_wall, is_oneway_L1, is_oneway_L2;

xx = argument0;
yy = argument1;
xx_add = argument2;
yy_add = argument3;

is_wall = place_meeting(xx + xx_add, yy + yy_add, obj_wall); //Check for walls
if (is_wall)
{
    return true;
}
if (argument3 >= 0) //Check for oneways only if there no movement or downwards movement
{
    is_oneway_L1 = id_collision_line(COL_DOWN, xx, yy, xx_add, yy_add - 1, obj_oneway) > 0;
    is_oneway_L2 = id_collision_line(COL_DOWN, xx, yy, xx_add, yy_add, obj_oneway) > 0;
    if ((not is_oneway_L1) and (is_oneway_L2))
    {
        return true;
    }
}
return false;
