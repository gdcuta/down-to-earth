//draw_effects()

/* This script is for visual effects due to statuses like invincibility flickering and possibly
    * overlaying colors for ailments (for example a red overlay for fire burns.  
    *
    * -Note that this must be placed in the draw event of the calling object.
    */
if is_invincible
{
    if timer_invincible > 0
    {
        if (timer_invincible mod 2 > 0)
        {
            if image_alpha = 1
            {
                image_alpha = 0
            }
            else
            {
                image_alpha = 1
            }
        }
        timer_invincible -= 1
    }
    if timer_invincible = 0
    {
        is_invincible = false
        image_alpha = 1
    }
}
