//id_collision_line(col_dir, xx, yy, xx_add, yy_add, object)

/* Checks if there is a collision at the edge of the mask either on it's top, bottom, left, or right side.
 * The xx and yy can be thought of as an offset to the entire bounding box and the xx_add and yy_add are
 * offsets of the sides relative to the initial offset.
*/
var dir_col, xx, yy, xx_add, yy_add, obj_col, lb, rb, tb, bb;

dir_col = argument0;
xx = argument1;
yy = argument2;
xx_add = argument3;
yy_add = argument4;
obj_col = argument5;

lb = round(xx) - sprite_get_xoffset(mask_index); //Left bound
rb = lb + sprite_get_width(mask_index) - 1; //Right bound
tb = round(yy) - sprite_get_yoffset(mask_index); //Top bound
bb = tb + sprite_get_height(mask_index) - 1; //Bottom Bound

switch (dir_col)
{
    case (COL_DOWN):  {return collision_line(round(lb + xx_add), round(bb + yy_add), round(rb + xx_add), round(bb + yy_add), obj_col, 1, 1); break;}
    case (COL_UP):    {return collision_line(round(lb + xx_add), round(tb + yy_add), round(rb + xx_add), round(tb + yy_add), obj_col, 1, 1); break;}
    case (COL_LEFT):  {return collision_line(round(lb + xx_add), round(tb + yy_add), round(lb + xx_add), round(bb + yy_add), obj_col, 1, 1); break;}
    case (COL_RIGHT): {return collision_line(round(rb + xx_add), round(tb + yy_add), round(rb + xx_add), round(bb + yy_add), obj_col, 1, 1); break;}
}
