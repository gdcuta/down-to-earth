//is_input(type,input,status)

/* This is to interpret the user's input
    * KEY[TYPE] = KEYBOARD,MOUSE,JOY1,JOY2
    * KEY[INPUT] = vk_space,mb_left,etc
    */
var type, input, status;
type = argument0;
input = argument1;
status = argument2;
switch(type)
{
    case(KEYBOARD):
    {
        switch(status)
        {
            case(HELD): {return keyboard_check(input); break;}
            case(PRESSED): {return keyboard_check_pressed(input); break;}
            case(RELEASED): {return keyboard_check_released(input); break;}
        }
        break;
    }
    case(MOUSE):
    {
        switch(status)
        {
            case(HELD): {return mouse_check_button(input); break;}
            case(PRESSED): {return mouse_check_button_pressed(input); break;}
            case(RELEASED): {return mouse_check_button_released(input); break;}
        }
        break;
    }
}
