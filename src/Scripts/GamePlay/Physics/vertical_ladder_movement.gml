var y_vel_temp;

y_vel_temp = argument0;

repeat(abs(y_vel_temp)) 
{
    
    if sign(y_vel_temp) < 0
    {
        if not place_meeting(x,y-1,obj_wall_parent) and place_meeting(x,y,obj_ladder)
        {
            y += sign(y_vel_temp);
        }
        else
        {
            y_vel = 0;
        }
    }
    else 
    {  
        if  not place_meeting(x,y+1,obj_wall_parent) and id_collision_line(COL_DOWN,x,y,0,1,obj_ladder) > 0
        {
            y += sign(y_vel_temp);
        }
    }
}
