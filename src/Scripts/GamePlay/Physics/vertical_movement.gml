//vertical_movement(y_vel_temp)

/* Moves the calling object based on y_vel_temp. This script returns true if the movement was
    * successful and false if it hits a wall.
    *
    * -Note that this is used, on the ground, in the air, and for moving the calling object via a lift.
    */
var y_vel_temp;

y_vel_temp = argument0;


repeat(abs(y_vel_temp)) 
{
    if (sign(y_vel_temp) < 0)
    {
        if (not is_place_meeting_platform(round(x), round(y), 0, -1))
        {
            y += sign(y_vel_temp);
        }
        else
        {
            y_vel = 0;
            return false;
        }
    }
    else 
    {
        lb = round(x) - sprite_get_xoffset(mask_index); //Left bound
        rb = lb + sprite_get_width(mask_index) - 1; //Right bound
        tb = round(y) - sprite_get_yoffset(mask_index); //Top bound
        bb = tb + sprite_get_height(mask_index) - 1; //Bottom Bound
        if (dir_face = DIR_LEFT and object_index = obj_player)
        {
            if (not is_on_ground and timer_grab = 0)
            {
                if (position_meeting(lb-2,tb+6,obj_wall_still_parent) and not position_meeting(lb-2,tb+5,obj_wall_still_parent)) 
                    or (position_meeting(lb-2,tb+6,obj_oneway_still_parent) and not position_meeting(lb-2,tb+5,obj_oneway_still_parent))
                {
                    y_vel = 0
                    is_hanging = true
                    return false
                }
            }
        }
        if (dir_face = DIR_RIGHT and object_index = obj_player)
        {
            if (not is_on_ground and timer_grab = 0)
            {
                if (position_meeting(rb+2,tb+6,obj_wall_still_parent) and not position_meeting(rb+2,tb+5,obj_wall_still_parent)) 
                    or (position_meeting(rb+2,tb+6,obj_oneway_still_parent) and not position_meeting(rb+2,tb+5,obj_oneway_still_parent))
                {
                    y_vel = 0
                    is_hanging = true
                    return false
                }
            }
        }
        if (not is_place_meeting_platform(round(x), round(y), 0, 1))
        {
            y += sign(y_vel_temp);
        }
    }
}
return true
