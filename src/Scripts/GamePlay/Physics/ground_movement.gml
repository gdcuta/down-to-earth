//Performs collisions and movements on a ground/surface
    //This includes moving lifts as well
if (is_on_ground)
{
    horizontal_movement(x_vel);   
    vertical_movement(y_vel);
    if ((not id_collision_line(COL_DOWN,x,y,0,1,OBJ_WALL) > 0) and (not id_collision_line(COL_DOWN,x,y,0,1,OBJ_ONEWAY) > 0))
    {
        is_on_ground = false;
        if (not is_jumping)
        {
            //Fall velocity is established in horizontal_movement
                //It is used so when you go down a slope and into the air you keep the downwards movement you had
            y_vel = y_vel_fall; 
        }
    }
}




