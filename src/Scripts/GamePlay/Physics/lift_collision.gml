var y_start, is_lift_found;
//x,y correction based on moving entities
//set x_vel and y_vel
is_vel_right = true;
is_vel_left = true;
is_vel_up = true;
is_vel_down = true;

if (inst_lift != noone and y_vel >= 0)
{
    if (object_is_ancestor(inst_lift.object_index,OBJ_WALL))
    {
        is_lift_L1 = id_collision_line(COL_DOWN,x,y,0,0,OBJ_WALL_LIFT) > 0;
        is_lift_L2 = id_collision_line(COL_DOWN,x,y,0,1,OBJ_WALL_LIFT) > 0;
        if ((not is_lift_L1) and (is_lift_L2))
        {
            is_lift_found = true;
        }
        else
        {
            is_lift_found = false;
        }
        if (not is_lift_found) //If you fall off a lift or the lift changes shape
        {
            obj_wall = OBJ_WALL_STILL;
            obj_oneway = OBJ_ONEWAY_STILL;
            y_start = y;
            if ((not is_lift_L1) and (not is_lift_L2)) //if you're above lift
            {
                repeat(DIST_CHECK_MAX )
                {
                    is_lift_L1 = id_collision_line(COL_DOWN,x,y,0,0,OBJ_WALL_LIFT) > 0;
                    is_lift_L2 = id_collision_line(COL_DOWN,x,y,0,1,OBJ_WALL_LIFT) > 0;
                    if (not is_lift_L1 and is_lift_L2)// or (is_lift_L1 and is_lift_L2)
                    {
                        is_lift_found = true;
                        break
                    }
                    if (vertical_movement(1) == false)
                    {
                        break
                    }
                }
                
            }
            if (is_lift_L1 and is_lift_L2) //if you've clipped in the lift
            {
                repeat(DIST_CHECK_MAX )
                {
                    is_lift_L1 = id_collision_line(COL_DOWN,x,y,0,0,OBJ_WALL_LIFT) > 0;
                    is_lift_L2 = id_collision_line(COL_DOWN,x,y,0,1,OBJ_WALL_LIFT) > 0;
                    if (not is_lift_L1 and is_lift_L2) //or (not is_lift_L1 and not is_lift_L2)
                    {
                        is_lift_found = true
                        break
                    }
                    if vertical_movement(-1) = false
                    {
                        break
                    }
                }
            }
            if (not is_lift_found)
            {
                y = y_start;
                is_on_lift = false;
                inst_lift = noone;
            }
            obj_wall = OBJ_WALL;
            obj_oneway = OBJ_ONEWAY;
       }
    }
    else if (object_is_ancestor(inst_lift.object_index,OBJ_ONEWAY))
    {
        is_lift_L1 = id_collision_line(COL_DOWN,x,y,0,0,OBJ_ONEWAY_LIFT) > 0;
        is_lift_L2 = id_collision_line(COL_DOWN,x,y,0,1,OBJ_ONEWAY_LIFT) > 0;
        if ((not is_lift_L1) and (is_lift_L2))
        {
            is_lift_found = true;
        }
        else
        {
            is_lift_found = false;
        }
        if (not is_lift_found) //If you fall off a lift or the lift changes shape
        {
            obj_wall = OBJ_WALL_STILL;
            obj_oneway = OBJ_ONEWAY_STILL;
            y_start = y;
            if ((not is_lift_L1) and (not is_lift_L2)) //if you're above lift
            {
                repeat(DIST_CHECK_MAX )
                {
                    is_lift_L1 = id_collision_line(COL_DOWN,x,y,0,0,OBJ_ONEWAY_LIFT) > 0;
                    is_lift_L2 = id_collision_line(COL_DOWN,x,y,0,1,OBJ_ONEWAY_LIFT) > 0;
                    if (not is_lift_L1 and is_lift_L2)// or (is_lift_L1 and is_lift_L2)
                    {
                        is_lift_found = true;
                        break
                    }
                    if (vertical_movement(1) == false)
                    {
                        break
                    }
                }
                
            }
            if (is_lift_L1 and is_lift_L2) //if you've clipped in the lift
            {
                repeat(DIST_CHECK_MAX )
                {
                    is_lift_L1 = id_collision_line(COL_DOWN,x,y,0,0,OBJ_ONEWAY_LIFT) > 0;
                    is_lift_L2 = id_collision_line(COL_DOWN,x,y,0,1,OBJ_ONEWAY_LIFT) > 0;
                    if (not is_lift_L1 and is_lift_L2) //or (not is_lift_L1 and not is_lift_L2)
                    {
                        is_lift_found = true
                        break
                    }
                    if vertical_movement(-1) = false
                    {
                        break
                    }
                }
            }
            if (not is_lift_found)
            {
                y = y_start;
                is_on_lift = false;
                inst_lift = noone;
            }
            obj_wall = OBJ_WALL;
            obj_oneway = OBJ_ONEWAY;
        }
    }
}

if (place_meeting(x,y,OBJ_WALL_LIFT))
{
    obj_wall = OBJ_WALL_STILL;
    inst_lift_left  = id_collision_line(COL_LEFT,x,y,0,0,OBJ_WALL_LIFT);
    inst_lift_right = id_collision_line(COL_RIGHT,x,y,0,0,OBJ_WALL_LIFT);
    inst_lift_up    = id_collision_line(COL_UP,x,y,0,0,OBJ_WALL_LIFT);
    inst_lift_down  = id_collision_line(COL_DOWN,x,y,0,0,OBJ_WALL_LIFT);
     
    is_lift_left  = inst_lift_left > 0;
    is_lift_right = inst_lift_right > 0;
    is_lift_up    = inst_lift_up > 0;
    is_lift_down  = inst_lift_down > 0;
    switch(is_lift_left + is_lift_right + is_lift_up + is_lift_down)
    {
        case(3):
        {
            
            if not is_lift_left
            {
                is_vel_right = false
                repeat(DIST_CHECK_MAX )
                {
                    if id_collision_line(COL_RIGHT,x,y,0,0,OBJ_WALL_LIFT) > 0 {if horizontal_movement(-1) = false {break}}
                    else {break}
                }
            }
            if not is_lift_right
            {
                is_vel_left = false
                repeat(DIST_CHECK_MAX )
                {
                    if id_collision_line(COL_LEFT,x,y,0,0,OBJ_WALL_LIFT) > 0 {if horizontal_movement(1) = false {break}}
                    else {break}
                }
            }
            if not is_lift_up
            {
                is_vel_down = false
                repeat(DIST_CHECK_MAX )
                {
                    if id_collision_line(COL_DOWN,x,y,0,0,OBJ_WALL_LIFT) > 0 {if vertical_movement(-1) = false {break}}
                    else {break}
                }
            }
            if not is_lift_down
            {
                is_vel_up = false
                repeat(DIST_CHECK_MAX )
                {
                    if id_collision_line(COL_UP,x,y,0,0,OBJ_WALL_LIFT) > 0 {if vertical_movement(1) = false {break}}
                    else {break}
                }
            }
            break;   
        
        }
        case(2):
        {
            Hmove = 0
            Vmove = 0
            repeat(DIST_CHECK_MAX ) {if id_collision_line(COL_RIGHT,x,y,Hmove,0,OBJ_WALL_LIFT) > 0 {Hmove-=1} else {break}}
            repeat(DIST_CHECK_MAX ) {if id_collision_line(COL_LEFT,x,y,Hmove,0,OBJ_WALL_LIFT) > 0 {Hmove+=1} else {break}}
            repeat(DIST_CHECK_MAX ) {if id_collision_line(COL_UP,x,y,0,Vmove,OBJ_WALL_LIFT) > 0 {Vmove+=1} else {break}}
            repeat(DIST_CHECK_MAX ) {if id_collision_line(COL_DOWN,x,y,0,Vmove,OBJ_WALL_LIFT) > 0 {Vmove-=1} else {break}}
            
            if abs(Vmove) >= abs(Hmove)
            {
                if Hmove > 0
                {
                    is_vel_left = false
                }
                if Hmove < 0
                {
                    is_vel_right = false
                }
                horizontal_movement(Hmove)
            }
            else
            {
                if Vmove > 0
                {
                    is_vel_up = false
                }
                if Vmove < 0
                {
                    is_vel_down = false
                }
               vertical_movement(Vmove) 
            }
            break;
            
        }
        case(1):
        {
            if is_lift_right
            {
                is_vel_right = false
                repeat(DIST_CHECK_MAX ) {if id_collision_line(COL_RIGHT,x,y,0,0,OBJ_WALL_LIFT) > 0 {horizontal_movement(-1)} else {break}}
        
            }
            if is_lift_left
            {
                is_vel_left = false
                repeat(DIST_CHECK_MAX ) {if id_collision_line(COL_LEFT,x,y,0,0,OBJ_WALL_LIFT) > 0 {horizontal_movement(1)} else {break}}
            }
            if is_lift_up
            {
                is_vel_up = false
                repeat(DIST_CHECK_MAX ) {if id_collision_line(COL_UP,x,y,0,0,OBJ_WALL_LIFT) > 0 {vertical_movement(-1)} else {break}}
            }
            if is_lift_down
            {
                is_vel_down = false
                repeat(DIST_CHECK_MAX ) {if id_collision_line(COL_DOWN,x,y,0,0,OBJ_WALL_LIFT) > 0 {vertical_movement(1)} else {break}}
            }
            break;
        }
    }
    obj_wall = OBJ_WALL 
}

