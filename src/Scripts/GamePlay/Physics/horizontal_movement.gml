//horizontal_movement(x_vel_temp)

/* Moves the calling object based on x_vel_temp. This script returns true if the movement was
    * successful and false if it hits a wall. Walls are defined by the height_slope_max variable, meaning if the
    * script has to check for ground higher/lower than the maximum slope height, then it assumes there's a wall/air.
    *
    * -Note that the height_slope_max is local to the calling object not this script so it needs to be defined beforehand.
    * -Note y_vel_fall is also local to the calling object and is used with y_start to determine if the calling object
    *  gained vertical velocity moving up or down a slope.
    * -Note that this is used, on the ground, in the air, and for moving the calling object via a lift.
    */
var was_moved, a, i, x_vel_temp, y_start;

x_vel_temp = argument0;
y_start = y;

repeat (ceil(abs(x_vel_temp)))
{
    was_moved = false;
    if (is_place_meeting_platform(round(x), round(y), sign(x_vel_temp), 0))
    {
        for (a = 1; a <= height_slope_max; a += 1)
        {
            if (not is_place_meeting_platform(round(x), round(y), sign(x_vel_temp), -a))
            {
                x += sign(x_vel_temp);
                y -= a;
                was_moved = true;          
                break;   
            }    
        }
        if (not was_moved) 
        {
            if (is_place_meeting_platform(round(x), round(y), sign(x_vel_temp), -height_slope_max))
            {
                x_vel_prev = x_vel_temp;
                x_vel = 0;
                return false;
            }
            else
            {
                for (a = height_slope_max; a >= 1; a -= 1)
                {
                    if (not is_place_meeting_platform(round(x), round(y), sign(x_vel_temp), a))
                    {
                        if (is_place_meeting_platform(round(x), round(y), sign(x_vel_temp), a + 1))
                        {
                            x += sign(x_vel_temp);
                            y += a;
                            was_moved = true;
                        }
                    }
                }
                if (not was_moved)
                {
                    x += sign(x_vel_temp);
                }
            }
        }
    }
    else 
    {
        for (a = height_slope_max; a >= 1; a -= 1)
        {
            if (not is_place_meeting_platform(round(x), round(y), sign(x_vel_temp), a))
            {
                if (is_place_meeting_platform(round(x), round(y), sign(x_vel_temp), a + 1))
                {
                    x += sign(x_vel_temp);
                    y += a;
                    was_moved = true; 
                }
            }
        }
        if (not was_moved)
        {
            x+= sign(x_vel_temp);
        }
    }
    if (is_on_ground and was_moved)
    {
        y_vel_fall = y-y_start;
    }
}
return true
