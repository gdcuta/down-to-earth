is_wall_L1 = id_collision_line(COL_DOWN,x,y,0,0,OBJ_WALL) > 0;
is_wall_L2 = id_collision_line(COL_DOWN,x,y,0,1,OBJ_WALL) > 0;
is_oneway_L1 = id_collision_line(COL_DOWN,x,y,0,0,OBJ_ONEWAY) > 0;
is_oneway_L2 = id_collision_line(COL_DOWN,x,y,0,1,OBJ_ONEWAY) > 0;

if ((not is_wall_L1 and is_wall_L2) or (not is_oneway_L1 and is_oneway_L2 and y_vel >= 0))
{
    is_on_ground = true;
    if y_vel>=0
    {
       is_jumping = false;
       y_vel = 0
    }
}
