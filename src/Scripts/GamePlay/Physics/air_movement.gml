//air_movement()

/* This script performs airbone collisions and movement based on current x_vel and y_vel
    * values. Obviously the calling object is assumed to be off the ground for anything here to happen.
    *
    * -Note that being on a ladder is implied to be in air due to no ground underneath.
    * -Note that the order is Step: air_movement->ground_movement->lift_movement
    *  EndStep: lift_collision->ground_collision
    */
if (not is_on_ground)
{
    horizontal_movement(x_vel);
    if (not is_on_ladder)
    {
        vertical_movement(y_vel);
    }
    else
    {
        vertical_ladder_movement(y_vel);
    } 
}
