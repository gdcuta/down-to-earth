//Performs lift collisions and move based on the lift movement
    //Will test to see if a lift is underneath and move character
inst_wall_lift_L1 = id_collision_line(COL_DOWN,x,y,0,0,OBJ_WALL_LIFT);
inst_wall_lift_L2 = id_collision_line(COL_DOWN,x,y,0,1,OBJ_WALL_LIFT);
inst_oneway_lift_L1 = id_collision_line(COL_DOWN,x,y,0,0,OBJ_ONEWAY_LIFT);
inst_oneway_lift_L2 = id_collision_line(COL_DOWN,x,y,0,1,OBJ_ONEWAY_LIFT);
if ((inst_wall_lift_L1 == noone) and (inst_wall_lift_L2 != noone)) //when you fall on a lift or get clipped to its edge this should happen
{
    is_on_lift = true;
    is_jumping = false;
    inst_lift = inst_wall_lift_L2
}
if ((inst_oneway_lift_L1 == noone) and (inst_oneway_lift_L2 != noone)) //when you fall on a lift or get clipped to its edge this should happen
{
    is_on_lift = true;
    is_jumping = false;
    inst_lift = inst_oneway_lift_L2
}
if (is_on_lift)
{
    obj_wall = OBJ_WALL_STILL
    horizontal_movement(inst_lift.x_vel)
    vertical_movement(inst_lift.y_vel)
    obj_wall = OBJ_WALL; 
}



       
