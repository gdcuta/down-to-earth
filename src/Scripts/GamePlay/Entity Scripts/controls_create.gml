//Controls variables - this can be used for keyboard inut or a.i. input
can_move_left = true;
can_move_right = true;
can_move_up = true;
can_move_down = true;
can_jump = true;

is_input_left = false;
is_input_right = false;
is_input_up = false;
is_input_down = false;
is_input_jump = false;
is_input_jump_previous = false;

switch(object_index)
{
    case(OBJ_PLAYER):
    {
        //Input Updater Variables - these are defining the player's controls 
            //from the controls_create() default values
        right_key[TYPE] = DEFAULT_RIGHT_KEY[TYPE];
        left_key[TYPE] = DEFAULT_LEFT_KEY[TYPE];
        down_key[TYPE] = DEFAULT_DOWN_KEY[TYPE];
        up_key[TYPE] = DEFAULT_UP_KEY[TYPE];
        jump_key[TYPE] = DEFAULT_JUMP_KEY[TYPE];
        attack_key[TYPE] = DEFAULT_ATTACK_KEY[TYPE];
        
        right_key[INPUT] = DEFAULT_RIGHT_KEY[INPUT];
        left_key[INPUT] = DEFAULT_LEFT_KEY[INPUT];
        down_key[INPUT] = DEFAULT_DOWN_KEY[INPUT];
        up_key[INPUT] = DEFAULT_UP_KEY[INPUT];
        jump_key[INPUT] = DEFAULT_JUMP_KEY[INPUT];
        attack_key[INPUT] = DEFAULT_ATTACK_KEY[INPUT];
        break;
    }
}
