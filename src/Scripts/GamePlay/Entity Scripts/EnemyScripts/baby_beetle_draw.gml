if -sign(x_vel) = 0
{
    sprFace = -dir_face
}
else 
{
    sprFace = sign(x_vel)
}
if is_invincible == false
{
    sprite_index = spr_baby_beetle
    image_speed = abs(x_vel)/4
    draw_sprite_ext(spr_baby_beetle,-1,x,y,sprFace,1,0,c_white,image_alpha)
}
else
{
    sprite_index = spr_damaged_beetle
    image_speed = abs(x_vel)/4
    draw_sprite_ext(spr_damaged_beetle,-1,x,y,sprFace,1,0,c_white,image_alpha)
}
draw_effects()
