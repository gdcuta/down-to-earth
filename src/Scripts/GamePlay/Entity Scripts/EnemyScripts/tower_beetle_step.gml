x_prev = xx
y_prev = yy
if state = 'crouching' or state = 'rising' or state = 'attacking'
{
    inst_leg_front_right.sprite_index = spr_tower_beetle_leg_front_bend
    inst_leg_front_left.sprite_index = spr_tower_beetle_leg_front_bend
    inst_leg_back_right.sprite_index = spr_tower_beetle_leg_front_bend
    inst_leg_back_left.sprite_index = spr_tower_beetle_leg_front_bend
    inst_leg_front_right.image_index = timer_crouch
    inst_leg_front_left.image_index = timer_crouch
    inst_leg_back_right.image_index = timer_crouch
    inst_leg_back_left.image_index = timer_crouch
    inst_leg_front_right.image_speed = 0
    inst_leg_front_left.image_speed = 0
    inst_leg_back_right.image_speed = 0
    inst_leg_back_left.image_speed = 0
}
xx_real = inst_bound_back.x+((inst_bound_front.x-inst_bound_back.x)/2)//*sign(inst_bound_front.x-inst_bound_back.x)
yy_real = inst_bound_back.y+((inst_bound_front.y-inst_bound_back.y)/2)//lbound.y+lengthdir_y(point_distance(lbound.x,lbound.y,rbound.x,rbound.y)/2,image_angle)+lengthdir_y(10,image_angle+90)

if state = 'walking' or state = 'turning'
{
    xx = xx_real
    yy = yy_real
    if timer_bounce >= 2 and timer_bounce < 3
    {
       yy = yy_real + 5
    }
    if timer_bounce >= 3 and timer_bounce < 4
    {
        yy = yy_real + 3
    }
    timer_bounce += .4
    if timer_bounce >= 7
    {
        timer_bounce = 0
    }
}
if state = 'crouching'
{
    if timer_crouch < 4
    {
        timer_crouch +=.2
    }
    if timer_crouch = 4
    {
        state = 'attacking'
    }
}
if state = 'rising'
{
    if timer_crouch > 0
    {
        timer_crouch -= .2
    }
    if timer_crouch = 0
    {
        state = 'walking'
        inst_leg_front_right.sprite_index = spr_tower_beetle_leg_front_walk
        inst_leg_front_left.sprite_index = spr_tower_beetle_leg_back_walk
        inst_leg_back_right.sprite_index = spr_tower_beetle_leg_front_walk
        inst_leg_back_left.sprite_index = spr_tower_beetle_leg_back_walk
        inst_leg_front_right.image_index = 0
        inst_leg_front_left.image_index = 4
        inst_leg_back_right.image_index = 4
        inst_leg_back_left.image_index = 0
        inst_bound_front.x_vel = walkspeed
        inst_bound_back.x_vel = walkspeed
    }
}
if state = 'crouching' or state = 'rising' or state = 'attacking'
{
    if timer_crouch >= 0 and timer_crouch < 1
    {
        
        yy = yy_real
        xx = xx_real
    }
    if timer_crouch >= 1  and timer_crouch < 2
    {
        yy = yy_real+3
        xx = xx_real+(-15)*dir_face
    }
    if timer_crouch >= 2 and timer_crouch < 3
    {
        yy = yy_real+18+3
        xx = xx_real+(-20-15)*dir_face
    }
    if timer_crouch >= 3 and timer_crouch < 4
    {
        yy = yy_real+33+18+3
        xx = xx_real+(-7-20-15)*dir_face
    }
    if timer_crouch >= 4 and timer_crouch < 5
    {
        yy = yy_real+14+33+18+3
        xx = xx_real+(10-7-20-15)*dir_face
    }
}



x += x_vel
y += y_vel

if inst_bound_front.x > inst_bound_back.x
{
    image_angle = point_direction(inst_bound_back.x,inst_bound_back.y,inst_bound_front.x,inst_bound_front.y)
}
else
{
    image_angle = point_direction(inst_bound_front.x,inst_bound_front.y,inst_bound_back.x,inst_bound_back.y)
}



