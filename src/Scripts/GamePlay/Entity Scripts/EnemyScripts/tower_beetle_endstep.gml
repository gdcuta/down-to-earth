inst_head_range = collision_rectangle(xx_real+(70*dir_face),yy_real-20,xx_real+(140*dir_face),yy_real+inst_bound_front.sprite_height,obj_player,false,true)
if state = 'walking'
{
    if abs(inst_bound_front.x-inst_bound_back.x)<140 && inst_bound_front.x_vel == 0
    {
        inst_bound_back.x += sign(inst_bound_front.x-inst_bound_front.x)
        inst_bound_back.x_vel = 0
    }
    
    if abs(inst_bound_back.x-inst_bound_back.x)<140 && inst_bound_back.x_vel == 0
    {
        inst_bound_front.x += sign(inst_bound_front.x-inst_bound_front.x)
        inst_bound_front.x_vel = 0
    }
    
    
    if inst_head_range != noone
    {
        state = 'crouching'
        timer_crouch = 0
    }
    
}
lb = round(x) - sprite_get_xoffset(mask_index); //Left bound
rb = lb + sprite_get_width(mask_index) - 1; //Right bound
tb = round(y) - sprite_get_yoffset(mask_index); //Top bound
bb = tb + sprite_get_height(mask_index) - 1; //Bottom Bound
if dir_face = DIR_RIGHT
    {     
        inst_head.x = rb
        inst_head.y = tb+20
    }
    else
    {   
        inst_head.x = lb
        inst_head.y = tb+20
    }
if state = 'turning'
{
    inst_bound_front.x_vel = -((sprite_width)/image_number)*(sin(degtorad(image_index*(180/image_number))))*dir_face
    inst_bound_back.x_vel= ((sprite_width)/image_number)*(sin(degtorad(image_index*(180/image_number))))*dir_face
    if image_index >= image_number-1
    {
        if dir_face = DIR_LEFT
        {
            dir_face = DIR_RIGHT
        }
        else
        {
            dir_face = DIR_LEFT
        }
        sprite_index = spr_tower_beetle_body
        state = 'walking'
        walkspeed *= -1
        inst_bound_front.x_vel = walkspeed
        inst_bound_back.x_vel = walkspeed
        
    }
    
}
if state = 'attacking' or state = 'crouching'
{
    inst_bound_front.x_vel = 0
    inst_bound_back.x_vel = 0
    if inst_head_range = noone
    {
        state = 'rising'
    }
}
inst_leg_front_right.x = inst_bound_front.x
inst_leg_front_right.y = inst_bound_front.y
inst_leg_front_left.x = inst_bound_front.x
inst_leg_front_left.y = inst_bound_front.y
inst_leg_back_right.x = inst_bound_back.x
inst_leg_back_right.y = inst_bound_back.y
inst_leg_back_left.x = inst_bound_back.x
inst_leg_back_left.y = inst_bound_back.y
if state = 'walking'
{
    image_speed = abs(walkspeed*.2)
    inst_leg_front_right.image_speed = abs(walkspeed*.1)
    inst_leg_front_left.image_speed = abs(walkspeed*.1)
    inst_leg_back_right.image_speed = abs(walkspeed*.1)
    inst_leg_back_left.image_speed = abs(walkspeed*.1)
}

if (dir_face = DIR_LEFT)
{
    inst_leg_front_right.image_xscale = -1
    inst_leg_front_left.image_xscale = -1
    inst_leg_back_right.image_xscale = -1
    inst_leg_back_left.image_xscale = -1
}
else
{
    inst_leg_front_right.image_xscale = 1
    inst_leg_front_left.image_xscale = 1
    inst_leg_back_right.image_xscale = 1
    inst_leg_back_left.image_xscale = 1
}
if state != 'turning'
{
    x_vel = xx-x_prev
    y_vel = yy-y_prev
}
if state = 'turning'
{
    
    x_vel = 0//inst_bound_front.x_vel
    
    y_vel = yy-y_prev
}
    

