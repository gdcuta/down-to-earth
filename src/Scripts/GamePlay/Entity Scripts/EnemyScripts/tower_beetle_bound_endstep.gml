ground_collision();

if x_vel = 0
{
    if body.state = 'walking'
    {
        body.state = 'turning'
        body.sprite_index = spr_tower_beetle_body_turn
        body.image_speed = 1
        body.image_index = 0;
        x_vel = ((body.sprite_width)/body.image_number)*(sin(degtorad(body.image_index*(180/body.image_number))))
        otherbound.x_vel = -((body.sprite_width)/body.image_number)*(sin(degtorad(body.image_index*(180/body.image_number))))
    }
    //body.walkspeed = 0
    //otherbound.x_vel = 0
}
if body.state = 'walking'
{
    x_vel = body.walkspeed
}
if (not is_on_ground) 
{
    y_vel += grav_world;
    y_vel = min(y_vel, y_vel_max);
    
    if (x_vel != 0)
    {
        x_vel = max(0, abs(x_vel) - fric_air)*sign(x_vel);     
    }
}
