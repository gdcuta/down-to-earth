dir_face = DIR_RIGHT;

state = 'walking';
has_turned = false

lb = round(x) - sprite_get_xoffset(mask_index); //Left bound
rb = lb + sprite_get_width(mask_index) - 1; //Right bound
tb = round(y) - sprite_get_yoffset(mask_index); //Top bound
bb = tb + sprite_get_height(mask_index) - 1; //Bottom Bound

inst_leg_front_right = instance_create(x+65,y+53,obj_tower_beetle_leg); //Creates front-right foot
inst_leg_front_left  = instance_create(x+65,y+53,obj_tower_beetle_leg); //Creates front-left foot
inst_leg_back_right  = instance_create(x-65,y+53,obj_tower_beetle_leg); //Creates back-right foot
inst_leg_back_left   = instance_create(x-65,y+53,obj_tower_beetle_leg); //Creates back-left foot

inst_leg_front_right.depth = depth - 1;
inst_leg_front_left.depth  = depth + 1;
inst_leg_back_right.depth  = depth - 1;
inst_leg_back_left.depth   = depth + 1;

inst_leg_front_left.sprite_index = spr_tower_beetle_leg_back_walk;
inst_leg_back_left.sprite_index  = spr_tower_beetle_leg_back_walk;
inst_leg_front_left.image_index = 4;
inst_leg_back_right.image_index = 4;

inst_head = instance_create(rb,tb+20,obj_tower_beetle_head)

walkspeed = 2
inst_bound_front = instance_create(rb,bb-20,obj_tower_beetle_bound)
inst_bound_back  = instance_create(lb,bb-20,obj_tower_beetle_bound)
inst_bound_front.force_move = 'none'
inst_bound_back.force_move = 'none'
inst_bound_back.otherbound  = inst_bound_front
inst_bound_front.otherbound = inst_bound_back
inst_bound_front.body = id
inst_bound_back.body  = id
inst_bound_front.x_vel = walkspeed
inst_bound_back.x_vel = walkspeed
x_prev = x
y_prev = y
timer_bounce = 0
timer_crouch = 0
x_vel =0
y_vel =0
xx=x
yy=y

