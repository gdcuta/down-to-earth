//Must be called before anything is created (I've put it in a room creation event before all the other rooms get created)
globalvar DIR_LEFT, DIR_RIGHT, 
    DEFAULT_RIGHT_KEY, DEFAULT_LEFT_KEY, DEFAULT_UP_KEY, DEFAULT_DOWN_KEY, DEFAULT_JUMP_KEY, DEFAULT_ATTACK_KEY, 
    KEYBOARD, MOUSE, 
    TYPE, INPUT, 
    COL_UP, COL_DOWN, COL_LEFT, COL_RIGHT, 
    HELD, PRESSED, RELEASED,
    OBJ_ENTITY, OBJ_PLAYER, OBJ_ENEMY, OBJ_WALL, OBJ_ONEWAY, 
    OBJ_WALL_STILL, OBJ_ONEWAY_STILL, OBJ_WALL_LIFT, OBJ_ONEWAY_LIFT,
    HEIGHT_SLOPE_MAX, DIST_CHECK_MAX ;

//Directions facing relative to the ground
DIR_LEFT = -1;
DIR_RIGHT = 1;

//Collision Directions for detecting things touching that side of the object's mask
COL_UP = 0;
COL_DOWN = 1;
COL_LEFT = 2;
COL_RIGHT = 3;

//Input properties KEY[PROPERTY] 
TYPE = 0;
INPUT = 1;

//Input types KEY[TYPE] = these
    // For example right_key[TYPE] = KEYBOARD
KEYBOARD = 0;
MOUSE = 1;
JOY1 = 2;
JOY2 = 3;

//Input status - These will be used in the is_input() script to determine if the key is being held, pressed, or released
HELD = 0;
PRESSED = 1;
RELEASED = 2;

//These are the defaults that will be passed to the player character later
DEFAULT_RIGHT_KEY[TYPE] = KEYBOARD;
DEFAULT_LEFT_KEY[TYPE] = KEYBOARD;
DEFAULT_UP_KEY[TYPE] = KEYBOARD;
DEFAULT_DOWN_KEY[TYPE] = KEYBOARD;
DEFAULT_JUMP_KEY[TYPE] = KEYBOARD;
DEFAULT_ATTACK_KEY[TYPE] = MOUSE;

DEFAULT_RIGHT_KEY[INPUT] = ord("D");
DEFAULT_LEFT_KEY[INPUT] = ord("A");
DEFAULT_UP_KEY[INPUT] = ord("W");
DEFAULT_DOWN_KEY[INPUT] = ord("S");
DEFAULT_JUMP_KEY[INPUT] = vk_space;
DEFAULT_ATTACK_KEY[INPUT] = mb_left;

OBJ_ENTITIY = obj_entity_parent;
OBJ_PLAYER = obj_player;
OBJ_ENEMY = obj_enemy_parent;
OBJ_WALL = obj_wall_parent;
OBJ_ONEWAY = obj_oneway_parent;
OBJ_WALL_STILL = obj_wall_still_parent;
OBJ_ONEWAY_STILL = obj_oneway_still_parent;
OBJ_WALL_LIFT = obj_wall_lift_parent;
OBJ_ONEWAY_LIFT = obj_oneway_lift_parent;

HEIGHT_SLOPE_MAX = 2;
DIST_CHECK_MAX = 64
