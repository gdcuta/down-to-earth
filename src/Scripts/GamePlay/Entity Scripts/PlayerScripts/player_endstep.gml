attack_routine();
take_damage();

lift_collision();
ground_collision();

player_input();
if is_hanging = false and is_climbing = false
{
    
    //Take control inputs - later we can hardcode input sequences for cutscenes
    if (is_input_right and is_vel_right)
    {
        x_vel = (min(x_vel_max, abs(x_vel) + fric_ground))
        dir_face = DIR_RIGHT;
    }
    if (is_input_left and is_vel_left)
    {
        x_vel = -(min(x_vel_max, abs(x_vel) + fric_ground))
        dir_face = DIR_LEFT;
    }
    
    //This is basically ladder detection - should probably be in it's own script
    if ((is_input_up and is_vel_up) and (id_collision_line(COL_DOWN,x,y,0,0,obj_ladder) > 0)) 
    {
        is_on_ground = false;
        is_on_ladder = true;
        y_vel = -3;
    }
    if ((is_input_down and is_vel_down) and (id_collision_line(COL_DOWN,x,y,0,1,obj_ladder) > 0))
    {
        is_on_ground = false;
        is_on_ladder = true;
        y_vel = 3;
    }
    if (not place_meeting(x,y+1,obj_ladder))
    {
        is_on_ladder = false;
    }
    if (not is_input_up and not is_input_down and is_on_ladder)
    {
        y_vel = 0
    }
    if (not is_input_left and not is_input_right and is_on_ladder)
    {
        x_vel = 0
    }
    
    //Ground friction
    if (is_on_ground) 
    {
        if (not is_jumping)
        {
            y_vel = 0;
        }
        
        if (x_vel != 0 and not is_input_left and not is_input_right) 
        {
            x_vel = max(0, abs(x_vel) - fric_ground)*sign(x_vel);
        }
    }
    //Air Friction and Gravity
    if (not is_on_ground and not is_on_ladder) 
    {
        y_vel += grav_world;
        y_vel = min(y_vel, y_vel_max);
        
        if (x_vel != 0)
        {
            x_vel = max(0, abs(x_vel) - fric_air)*sign(x_vel);     
        }
    }
    
    //Jumping
    if (is_input_jump)
    {
        if ((is_on_ground or is_on_ladder) and is_input_jump_pressed) 
        {
            is_on_ground = false;
            is_on_ladder = false;
            is_jumping = true;
            y_vel = -vel_jump;
        }
        //This causes gravity to affect the jump as soon as you release the key (for shorter jumps)
        else if not is_on_ground and sign(y_vel) == -1
        {
            y_vel -= grav_world/2;
        }
    }
    else
    {    
        is_jumping = false;
    }
    if not is_vel_up and y_vel <0
    {
        y_vel = 0
    }
}

if is_hanging = true
{   
    timer_grab = 5
    if is_input_up
    {
        is_climbing = true
        is_hanging = false
    }
    if is_input_down
    {
        is_hanging = false
        is_releasing = true
    }
}
if timer_grab > 0
{
    timer_grab -=1
}
if is_climbing = true
{
    if is_snapping = false
    {
        if dir_face = DIR_LEFT
        {
            yadd = 0
            repeat(64)
            {
                if place_meeting(lb-2,y+yadd,obj_wall_still_parent) or place_meeting(lb-2,y+yadd,obj_oneway_still_parent)
                {
                    yadd -= 1
                }
                else
                {
                    xsnap = lb-2
                    ysnap = y+yadd
                    is_snapping = true
                    break
                }
            }
        }
        if dir_face = DIR_RIGHT
        {
            yadd = 0
            repeat(64)
            {
                if place_meeting(rb+2,y+yadd,obj_wall_still_parent) or place_meeting(rb+2,y+yadd,obj_oneway_still_parent)
                {
                    yadd -= 1
                }
                else
                {
                    xsnap = rb+2
                    ysnap = y+yadd
                    is_snapping = true
                    break
                }
            }
        }
    }
    else
    {
        x += lengthdir_x(5,point_direction(x,y,xsnap,ysnap))
        y += lengthdir_y(5,point_direction(x,y,xsnap,ysnap))
        if point_distance(x,y,xsnap,ysnap) <= 5
        {
            x = xsnap
            y = ysnap
            is_climbing = false
            is_snapping = false
        }
    }
}
wrench_draw()

