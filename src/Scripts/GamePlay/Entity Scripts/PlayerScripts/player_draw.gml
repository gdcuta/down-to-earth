image_speed = .3;
if (dir_face == DIR_LEFT)
{
    if (is_on_ground)
    {
        if (x_vel == 0) {sprite_index = spr_player_stand_left;}
        if (x_vel < 0) {sprite_index = spr_player_run_left;}
    }
    else
    {
        if (is_on_ladder) {sprite_index = spr_player_stand_left;}
        if (is_hanging) {sprite_index = spr_player_ledge_left; image_speed = 0; image_index = 0}
        if (is_climbing) {image_speed = .4}
        if (not is_on_ladder and not is_hanging and not is_climbing) {sprite_index = spr_player_jump_left;}
    }
}
if (dir_face == DIR_RIGHT)
{
    if (is_on_ground)
    {
        if (x_vel == 0) {sprite_index = spr_player_stand_right;}
        if (x_vel > 0) {sprite_index = spr_player_run_right;}
    }
    else
    {
        if (is_on_ladder) {sprite_index = spr_player_stand_right;}
        if (is_hanging) {sprite_index = spr_player_ledge_right; image_speed = 0; image_index = 0}
        if (is_climbing) {image_speed = .4}
        if (not is_on_ladder and not is_hanging and not is_climbing) {sprite_index = spr_player_jump_right;}
    }
}

draw_effects();
draw_sprite_ext(sprite_index,-1,x,y,1,1,0,c_white,image_alpha);
inst_wrench.image_alpha = image_alpha
//draw_sprite(spr_player_mask,-1,x,y);


