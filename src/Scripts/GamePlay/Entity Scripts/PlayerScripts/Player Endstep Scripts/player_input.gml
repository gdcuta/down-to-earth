//------------------------------------scr_input(key type       , key input       , key status)
if (can_move_right) {is_input_right = is_input(right_key[TYPE], right_key[INPUT], HELD);}
if (can_move_left)  {is_input_left  = is_input(left_key[TYPE] , left_key[INPUT] , HELD);}
if (can_move_up)    {is_input_up    = is_input(up_key[TYPE]   , up_key[INPUT]   , HELD);}
if (can_move_down)  {is_input_down  = is_input(down_key[TYPE] , down_key[INPUT] , HELD);}

if (is_input_right and is_input_left)
{
    is_input_right = false;
    is_input_left = false;
}

is_input_jump_previous = is_input_jump;

if (can_jump)
{
    is_input_jump = is_input(jump_key[TYPE],jump_key[INPUT], HELD);
    is_input_jump_pressed = is_input(jump_key[TYPE],jump_key[INPUT], PRESSED);
}
