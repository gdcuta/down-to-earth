if (is_on_ground and place_meeting(x,y,obj_armor_beetle))
{
    x_velocity = instance_nearest(x,y,obj_armor_beetle).x_velocity;
}

if (is_on_ground and place_meeting(x,y,obj_baby_beetle))
{
    x_velocity = sign(x-instance_nearest(x,y,obj_baby_beetle))*2;
}
