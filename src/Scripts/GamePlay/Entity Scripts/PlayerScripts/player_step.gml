//player_step()

/* This is the player's main step event. (This will look really similar to most entity
    * step events as they share the same physics)
    */
    
x_prev = x;
y_prev = y;
mask_index = spr_player_mask;

obj_wall = OBJ_WALL_STILL;
air_movement();
ground_movement();
obj_wall = OBJ_WALL;
lift_movement();
