if (dir_face == DIR_RIGHT)
{
    flip = 1;
    ang = 0;
}
else
{
    flip = -1
    ang = 180
}
inst_wrench.image_xscale=1
inst_wrench.depth = depth-1
if is_attacking = true
{
    if ang_attack > 50
    {
        is_attacking = false
        ang_attack = 0
    }
    else
    {
        inst_wrench.image_angle = (ang+270)+((40-ang_attack)*flip)
        ang_attack += 5
        inst_wrench.x = x-(1*flip)
        inst_wrench.y = y+3
    }
}
else
{
    switch(sprite_index)
    {
        case(spr_player_run_right):
        case(spr_player_run_left):
        {
            
            
            if (image_index > 0 and image_index <= 1)
            {
                inst_wrench.image_angle = (ang+270)+(0*flip)
                inst_wrench.x = x-(1*flip)
                inst_wrench.y = y+3
            }
            if (image_index > 1 and image_index <= 2)
            {
                inst_wrench.image_angle = (ang+270)+(-20*flip)
                inst_wrench.x = x-(5*flip)
                inst_wrench.y = y+3
            }
            if (image_index > 2 and image_index <= 3)
            {
                inst_wrench.image_angle = (ang+270)+(0*flip)
                inst_wrench.x = x-(1*flip)
                inst_wrench.y = y+3
            }
            if (image_index > 3 and image_index <= 4)
            {
                inst_wrench.image_angle = (ang+270)+(30*flip)
                inst_wrench.x = x+(10*flip)
                inst_wrench.y = y
            }
            if (image_index > 4 and image_index <= 5)
            {
                inst_wrench.image_angle = (ang+270)+(40*flip)
                inst_wrench.x = x+(14*flip)
                inst_wrench.y = y-3
            }
            if (image_index > 5 and image_index <= 6)
            {
                inst_wrench.image_angle = (ang+270)+(30*flip)
                inst_wrench.x = x+(10*flip)
                inst_wrench.y = y
            }
            break;
        }
        case(spr_player_stand_right):
        case (spr_player_stand_left):
        {
            inst_wrench.image_angle = (ang+270)+(0*flip);
            inst_wrench.x = x-(1*flip);
            inst_wrench.y = y+3;
            break;
        }
        case(spr_player_jump_right):
        case(spr_player_jump_left):
        {
            if (image_index > 0 and image_index <=1)
            {
                inst_wrench.image_angle = (ang+270)+(-20*flip)
                inst_wrench.x = x-(2*flip)
                inst_wrench.y = y+4
            }
            if (image_index > 1 and image_index <=2)
            {
                inst_wrench.image_angle = (ang+270)+(-50*flip)
                inst_wrench.x = x-(2*flip)
                inst_wrench.y = y+6
            }
            break;   
        }
        default:
        {
            inst_wrench.image_angle = 270
            inst_wrench.x = x-1
            inst_wrench.y = y+3
        }
    }
}


