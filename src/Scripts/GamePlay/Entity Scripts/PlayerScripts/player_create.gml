//Stuff to initiate when the player is created
event_inherited(); 
advanced_physics_create();
controls_create();
combat_create();

//Drawing Variables
image_speed = .3;

inst_wrench = instance_create(x,y,obj_player_wrench);
inst_wrench.depth = depth-1;
inst_wrench.inst_player = id;
instance_create(x,y,obj_gui_controller);

timer_grab = 0
