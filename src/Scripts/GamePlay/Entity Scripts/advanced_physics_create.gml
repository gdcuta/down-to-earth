//All Players, NPCS, and Enemies - Create advanced physics variables (for walking, jumping)

is_on_ground = false;
is_in_water = false;
is_on_lift = false;
is_jumping = false;
is_on_ladder = false;

dir_face = DIR_RIGHT;

fric_air = 0.15;
fric_ground = 0.85;
grav_world = 2;
height_slope_max = HEIGHT_SLOPE_MAX;

x_vel_add = 0;
y_vel_add = 0;
x_vel_max = 5;
y_vel_max = 16;
y_vel_fall = 0;

obj_wall = OBJ_WALL;
obj_oneway = OBJ_ONEWAY;
inst_lift = noone;

switch(object_index)
{
    case(OBJ_PLAYER): //Variables for players only and overrides
    {
        vel_jump = 16;
        current_hp = 10;
        is_on_ladder = false;
        is_hanging = false;
        is_climbing = false;
        is_snapping = false;
    }
}
