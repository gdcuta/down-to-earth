if place_meeting(x,y,ct_jump_up) and is_on_ground = true and spot_active == true
{
    y_vel = -20
    is_jumping = true
    spot_active = false

    
}
else if place_meeting(x,y,ct_jump_right) and is_on_ground = true and spot_active == true
{
    y_vel = -20
    x_vel = 10
    sprFace = 1
    is_jumping = true
    spot_active = false

}
else if place_meeting(x,y,ct_jump_left) and is_on_ground = true and spot_active == true
{
    y_vel = -20
    x_vel = -10
    sprFace = -1
    is_jumping = true
    spot_active = false
 
}
else
{
    x_vel = 2 * sign(x_vel)
}
if spot_active == false
{
    control_spot_counter += 1
    if control_spot_counter == 60
    {
        spot_active = true
        control_spot_counter = 0
    }

}
