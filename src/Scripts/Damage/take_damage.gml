if (not is_invincible)
{
    switch(object_index)
    {
        case(obj_baby_beetle):
        {
            inst_wrench = instance_place(x,y,obj_player_wrench)
            if (inst_wrench != noone)
            {
                //Will put formulas to calucate damage bassed on wrench properties here
                if inst_wrench.inst_player.is_attacking = true
                {
                    is_harmed = true
                    current_hp -= inst_wrench.inst_player.attack;
                    if x >= inst_wrench.inst_player.x
                    {
                        x_vel += 2
                    }
                    else
                    {
                        x_vel -= 2
                    }
                }
            }
            break;
        }
        case(obj_armor_beetle):
        {
            inst_wrench = instance_place(x,y,obj_player_wrench)
            if (inst_wrench != noone)
            {
                //Will put formulas to calucate damage bassed on wrench properties here
                if inst_wrench.inst_player.is_attacking = true
                {
                    is_harmed = true
                    current_hp -= inst_wrench.inst_player.attack;
                    if x >= inst_wrench.inst_player.x
                    {
                        x_vel += 2
                    }
                    else
                    {
                        x_vel -= 2
                    }
                }
            }
            break;
        }                               
        case(OBJ_PLAYER):
        {    
        
            
        }
    }
}

if (is_harmed and not is_invincible)
{
    if current_hp <= 0
    {
        if object_index = obj_baby_beetle
        {
            instance_create(x,y,obj_dead_baby_beetle)
            
        }
            
        instance_destroy()
    }
    else
    {
        is_harmed = false
        is_invincible = true
        timer_invincible = num_invincible_frames;
    }
}
