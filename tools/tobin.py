#!/usr/bin/env python

from os import remove, rename, startfile
from os.path import isfile

from project import project_join
from gmksplitter import dir_to_gmk

backup = project_join('bin/Down to Earth.tmp.gb0')
target = project_join('bin/Down to Earth.tmp.gm81')

if isfile(backup):
    remove(backup)
if isfile(target):
    rename(target, backup)

dir_to_gmk(project_join('src'), target, overwrite=True)

startfile(target)
