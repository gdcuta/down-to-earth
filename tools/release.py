#!/usr/bin/env python

import xml.etree.ElementTree as ET

from os import remove
from project import project_join
from gmksplitter import dir_to_gmk

tree = ET.parse(project_join('src/Global Game Settings.xml'))
tree._setroot(tree.find('gameInfo'))
version = tree.findtext('version')

target = dir_to_gmk(project_join('src'), project_join('bin/Down to Earth.{ver}.gm81'.format(ver=version)))
if target.endswith('.tmp.gm81'):
    remove(target)
else:
    # commit to git
    # tag commit with version

    # upload to bitbucket
