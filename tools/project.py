from os.path import join, realpath, split

__all__ = ['project_join', 'PROJECTPATH']

PROJECTPATH = realpath(join(split(__file__)[0], '..'))

def project_join(*args):
    return join(PROJECTPATH, *args)
