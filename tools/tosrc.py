#!/usr/bin/env python

from project import project_join
from gmksplitter import gmk_to_dir

gmk_to_dir(project_join('bin/Down to Earth.tmp.gm81'), project_join('src'), overwrite=True)
