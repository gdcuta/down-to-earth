#!/usr/bin/env python

from project import get_version, get_version_str, project_join, set_version

def increment_version(key):
    if key == 'major':
        increment_major()
    elif key == 'minor':
        increment_minor()
    elif key == 'release':
        increment_release()
    elif key == 'build':
        increment_build()
    else:
        pass

def increment_major():
    version = get_version()
    version['major'] += 1
    version['minor'] = 0
    version['release'] = 0
    version['build'] = 0
    set_version(version)

def increment_minor():
    version = get_version()
    version['minor'] += 1
    version['release'] = 0
    version['build'] = 0
    set_version(version)

def increment_release():
    version = get_version()
    version['release'] += 1
    version['build'] = 0
    set_version(version)

def increment_build():
    version = get_version()
    version['build'] += 1
    set_version(version)

if __name__ == '__main__':
    from argparse import ArgumentParser
    
    parser = ArgumentParser(description='Aids in incrementing the game\'s version number')
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-M', '--major', action='store_true', help='Increment the game\'s major version number.')
    group.add_argument('-m', '--minor', action='store_true', help='Increment the game\'s minor version number.')
    group.add_argument('-r', '--release', action='store_true', help='Increment the game\'s release version number.')
    group.add_argument('-b', '--build', action='store_true', help='Increment the game\'s build version number.')
    args = parser.parse_args()
    
    print 'Current version:', get_version_str()
    
    if args.major:
        increment_major()
    elif args.minor:
        increment_minor()
    elif args.release:
        increment_release()
    elif args.build:
        increment_build()
    else:
        increment_version(raw_input('Enter "major", "minor", "release", or "build" to increment version: '))
    
    print 'New version:    ', get_version_str()
