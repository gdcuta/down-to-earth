#!/usr/bin/env python

# Version 2.1

# The MIT License (MIT)
# Copyright (c) 2014 Jacob McClenny <https://bitbucket.org/JacobMcClenny>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

"""Automates the use of the GmkSplitter tool

Requires:
    * Java installed
    * The Java bin in the command shell path
    * The file gmksplit.jar in the same directory as this script

"""

from errno import ENOENT, ENOTDIR
from os import remove, rename, strerror
from os.path import isdir, isfile, join, realpath, split, splitext
from shutil import rmtree
from subprocess import call
from time import strftime

__all__ = ['dir_to_gmk', 'gmk_to_dir', 'gmksplit']

_DEFAULT_VERSION = '8.1'
_JARPATH = join(split(__file__)[0], 'gmksplit.jar')

def dir_to_gmk(source, target=None, version=_DEFAULT_VERSION, overwrite=False):
    """Merge a directory into a GameMaker project file

    If no target is provided, the source directory name appended with an
    extension will be used. The extension will be based on the provided
    version. Will create a temporary file named the same as the target, but
    with a datetime stamp appended to the end. If overwrite is allowed, will
    delete the target file if it already exists. Lastly renames the
    temporary file to be the target file if the target file does not exist.

    Args:
        source (str): The directory to be merged.
        target (str): The file to be created.
        version (str): The target version of GameMaker.
        overwrite (bool): Whether to overwrite the target file if it already
            exists.

    Returns:
        The path to the file created.

    Raises:
        IOError: The source is not an existing directory.

    """
    if isdir(source):
        extension = splitext(target)[1] if splitext(target)[1] else '.gm81' if version == '8.1' else '.gmk'
        dirpath = realpath(source)
        filepath = realpath(target) if target else dirpath + extension
        filetemp = '{}_{}{}'.format(splitext(filepath)[0], strftime('%Y%m%d%H%M%S'), extension)
        call('java -jar "{jar}" "{dir}" "{file}"'.format(jar=_JARPATH, dir=dirpath, file=filetemp))
        if overwrite and isfile(filepath):
            remove(filepath)
        if isfile(filepath):
            return filetemp
        else:
            rename(filetemp, filepath)
            return filepath
    else:
        raise IOError(ENOTDIR, strerror(ENOTDIR), source)

def gmk_to_dir(source, target=None, overwrite=False):
    """Split a GameMaker project file into a directory format

    If no target is provided, the source filename with its extension removed
    will be used. Will create a temporary directory named the same as the
    target, but with a datetime stamp appended to the end. If overwrite is
    allowed, will delete the target directory if it already exists. Lastly
    renames the temporary directory to be the target directory if the target
    directory does not exist.

    Args:
        source (str): The file to be split.
        target (str): The directory to be created.
        overwrite (bool): Whether to overwrite the target directory if it
            already exists.

    Returns:
        The path to the directory created.

    Raises:
        IOError: The source is not an existing file.

    """
    if isfile(source):
        filepath = realpath(source)
        dirpath = realpath(target) if target else splitext(filepath)[0]
        dirtemp = '{}_{}'.format(dirpath, strftime('%Y%m%d%H%M%S'))
        call('java -jar "{jar}" "{file}" "{dir}"'.format(jar=_JARPATH, file=filepath, dir=dirtemp))
        if overwrite and isdir(dirpath):
            rmtree(dirpath)
        if isdir(dirpath):
            return dirtemp
        else:
            rename(dirtemp, dirpath)
            return dirpath
    else:
        raise IOError(ENOENT, strerror(ENOENT), source)

def gmksplit(source, target=None, version=_DEFAULT_VERSION, overwrite=False):
    """Split a GameMaker project file into a directory format or vice versa

    Calls dir_to_gmk() or gmk_to_dir() as appropriate based on whether
    source is a directory or file.

    Args:
        source (str): The directory or file to be split or merged.
        target (str): The file or directory to be created.
        version (str): The target version of GameMaker.
        overwrite (bool): Whether to overwrite the target if it already
            exists.

    Returns:
        The path to the directory or file created.

    Raises:
        IOError: The source is not an existing file or a directory.

    """
    if isdir(source):
        return dir_to_gmk(source, target, version, overwrite)
    elif isfile(source):
        return gmk_to_dir(source, target, overwrite)
    else:
        raise IOError(ENOENT, strerror(ENOENT), source)

if __name__ == '__main__':
    from argparse import ArgumentParser
    
    parser = ArgumentParser(description='Automatically split a GameMaker project file into a directory format or merge a directory into a GameMaker project file.')
    parser.add_argument('source', help='The project file or directory to be split or merged.')
    parser.add_argument('target', nargs='?', default=None, help='(Optional) The name of the output directory or file.')
    parser.add_argument('-v', '--version', default=_DEFAULT_VERSION, choices=['8.0', '8.1'], help='Target version of GameMaker.')
    parser.add_argument('-o', '--overwrite', action='store_true', help='Overwrite target if it already exists.')
    args = parser.parse_args()
    
    gmksplit(args.source, args.target, args.version, args.overwrite)
